package br.com.indra.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket ApiDoc() {
		Docket docket = new Docket(DocumentationType.SWAGGER_2);		 
		docket.select()
		.apis(RequestHandlerSelectors.basePackage("br.com.indra"))
		.paths(PathSelectors.any())
		.build()
		.apiInfo(this.informacoesApi().build());
 
		return docket;
	}
 
	private ApiInfoBuilder informacoesApi() {
 
		ApiInfoBuilder apiInfoBuilder = new ApiInfoBuilder();
 
		apiInfoBuilder.title("Api Combustivel");
		apiInfoBuilder.description("Api para realização de um CRUD - combustivel.");
		apiInfoBuilder.version("1.0");
		apiInfoBuilder.termsOfServiceUrl("Termo de uso: Deve ser usada para avaliação.");
		apiInfoBuilder.license("Licença - Open Source");
		apiInfoBuilder.licenseUrl("");
		apiInfoBuilder.contact(contato());
 
		return apiInfoBuilder;
 
	}
	private Contact contato() {
 
		return new Contact(
				"Hélio Alves",
				"", 
				"halvesd@indracompany.com");
	}

}
