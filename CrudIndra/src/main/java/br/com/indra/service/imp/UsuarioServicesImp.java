package br.com.indra.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.indra.dto.UsuarioDto;
import br.com.indra.entity.Usuario;
import br.com.indra.repository.IUsuarioRepository;
import br.com.indra.service.IUsuarioService;

@Service
@Transactional
public class UsuarioServicesImp implements IUsuarioService {

	private IUsuarioRepository iUsuarioRepository;
	
	@Autowired
	public UsuarioServicesImp(IUsuarioRepository iUsuarioRepository) {
		this.iUsuarioRepository = iUsuarioRepository;
	}
	
	@Override
	public List<Usuario> listarTodos() {
		return (List<Usuario>) iUsuarioRepository.findAll();
	}

	@Override
	public Usuario buscarPorId(Long id) {
		return iUsuarioRepository.getOne(id);
	}

	@Override
	public Usuario cadastrar(Usuario usuario) {
		return iUsuarioRepository.save(usuario);
	}

	@Override
	public Usuario atualizar(Usuario usuario) {
 		return iUsuarioRepository.save(usuario);
	}

	@Override
	public void remover(Long id) {
		iUsuarioRepository.delete(iUsuarioRepository.getOne(id));		
	}

	@Override
	public UsuarioDto buscarPorCPF(String cpf) {
		return iUsuarioRepository.buscarPorCPF(cpf);
	}

}
