package br.com.indra.service.imp;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import br.com.indra.dto.BandeiraDto;
import br.com.indra.dto.MunicipioDto;
import br.com.indra.dto.ProdutoDto;
import br.com.indra.entity.Produto;
import br.com.indra.repository.IProdutoRepository;
import br.com.indra.service.IProdutoService;
import br.com.indra.utils.DataUtils;

@Service
@Transactional
public class ProdutoServiceImp implements IProdutoService {

	private ProdutoDto produtoDto;

	private ArrayList<ProdutoDto> listaProdutoDTO;

	private IProdutoRepository iProdutoRepository;

	@Autowired
	public ProdutoServiceImp(IProdutoRepository iHistoricoRepository) {
		this.iProdutoRepository = iHistoricoRepository;
	}

	@Override
	public List<Produto> listarTodos() {
		return (List<Produto>) iProdutoRepository.findAll();
	}

	@Override
	public Produto buscarPorId(Long id) {
		return iProdutoRepository.getOne(id);
	}

	@Override
	public Produto cadastrar(Produto produto) {
		return iProdutoRepository.save(produto);
	}

	@Override
	public Produto atualizar(Produto produto) {
		return iProdutoRepository.save(produto);
	}

	@Override
	public void remover(Long id) {
		iProdutoRepository.deleteById(id);
	}

	@Override
	public List<ProdutoDto> importaArquivoCsv(MultipartFile file) {

		InputStream inputStream = null;
		Scanner leitor = null;
		try {
			inputStream = file.getInputStream();
			leitor = new Scanner(inputStream);

			// variavel que armazenara as linhas do arquivo
			String linhasDoArquivo = new String();

			listaProdutoDTO = new ArrayList<ProdutoDto>();
			// percorre todo o arquivo
			while (leitor.hasNext()) {

				// recebe cada linha do arquivo
				linhasDoArquivo = leitor.nextLine();

				// separa os campos entre os espaços duplo de cada linha
				String[] valoresEntreDoisEspacos = linhasDoArquivo.split("  "); // divisor

				if (!(valoresEntreDoisEspacos[0].trim().length() > 2)) {
					String regiao = valoresEntreDoisEspacos[0];
					String siglaestado = (valoresEntreDoisEspacos[1].isEmpty()) ? null
							: valoresEntreDoisEspacos[1].trim();
					String siglamunicipio = (valoresEntreDoisEspacos[2].isEmpty()) ? null
							: valoresEntreDoisEspacos[2].trim();
					String distribuidora = (valoresEntreDoisEspacos[3].isEmpty()) ? null
							: valoresEntreDoisEspacos[3].trim();
					String coproduto = (valoresEntreDoisEspacos[4].isEmpty()) ? null
							: valoresEntreDoisEspacos[4].trim();
					String noproduto = (valoresEntreDoisEspacos[5].isEmpty()) ? null
							: valoresEntreDoisEspacos[5].trim();
					Date datacoleta = (valoresEntreDoisEspacos[6].isEmpty()) ? null
							: DataUtils.converteData(valoresEntreDoisEspacos[6].trim(), "dd/MM/yyyy");
					Double valorcompra = (valoresEntreDoisEspacos[7].isEmpty()) ? 0
							: Double.parseDouble(valoresEntreDoisEspacos[7].trim().replace(',', '.'));
					Double valorvenda = (valoresEntreDoisEspacos[8].isEmpty()) ? 0
							: Double.parseDouble(valoresEntreDoisEspacos[8].trim().replace(',', '.'));
					String unidademedida = (valoresEntreDoisEspacos[9].isEmpty()) ? null
							: valoresEntreDoisEspacos[9].trim();
					String bandeira = (valoresEntreDoisEspacos[10].isEmpty()) ? null
							: valoresEntreDoisEspacos[10].trim();

					produtoDto = new ProdutoDto(regiao, siglaestado, siglamunicipio, distribuidora, coproduto,
							noproduto, datacoleta, valorcompra, valorvenda, unidademedida, bandeira);
					listaProdutoDTO.add(produtoDto);
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				inputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return listaProdutoDTO;
	}

	@Override
	public List<MunicipioDto> precoMedioCompraCombustivelMunicipio() {
		List<MunicipioDto> lista = iProdutoRepository.precoMedioCompraCombustivelMunicipio();
		return lista;
	}

	@Override
	public List<MunicipioDto> precoMedioCompraVendaCombustivelMunicipio() {
		List<MunicipioDto> lista = iProdutoRepository.precoMedioCompraVendaCombustivelMunicipio();
		return lista;
	}

	@Override
	public List<BandeiraDto> precoMedioCompraVendaCombustivelBancdeira() {
		List<BandeiraDto> lista = iProdutoRepository.precoMedioCompraVendaCombustivelBancdeira();
		return lista;
	}

	@Override
	public Map<String, List<Produto>> getExibeInformacaoImportadaSiglaPorRegiao() {
		Map<String, List<Produto>> collect = iProdutoRepository.findAll().stream().collect(Collectors.groupingBy(Produto::getSiglaregiao));
		return collect;
	}

	@Override
	public Map<String, List<Produto>> getExibeInformacaoImportadaDistribuidora() {
		Map<String, List<Produto>> collect = iProdutoRepository.findAll().stream().collect(Collectors.groupingBy(Produto::getDistribuidora));
		return collect;
	}
	
	@Override
	public Map<Date, List<Produto>> getExibeInformacaoImportadaDataColeta() {
		Map<Date, List<Produto>> collect = iProdutoRepository.findAll().stream().collect(Collectors.groupingBy(Produto::getDatacoleta));
		return collect;
	}

}
