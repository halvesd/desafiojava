package br.com.indra.service;

import java.util.List;

import br.com.indra.dto.UsuarioDto;
import br.com.indra.entity.Usuario;

public interface IUsuarioService {

	UsuarioDto buscarPorCPF(String cpf);
	
	List<Usuario> listarTodos();
	
	Usuario buscarPorId(Long id);
	
	Usuario cadastrar(Usuario usuario);
	
	Usuario atualizar(Usuario usuario);
	
	void remover(Long id);
}
