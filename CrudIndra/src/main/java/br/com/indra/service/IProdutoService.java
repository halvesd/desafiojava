package br.com.indra.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import br.com.indra.dto.BandeiraDto;
import br.com.indra.dto.MunicipioDto;
import br.com.indra.dto.ProdutoDto;
import br.com.indra.entity.Produto;;

public interface IProdutoService {

	List<Produto> listarTodos();

	Produto buscarPorId(Long id);

	Produto cadastrar(Produto produto);

	Produto atualizar(Produto produto);

	void remover(Long id);

	List<ProdutoDto> importaArquivoCsv(MultipartFile file);

	List<MunicipioDto> precoMedioCompraCombustivelMunicipio();

	List<MunicipioDto> precoMedioCompraVendaCombustivelMunicipio();

	List<BandeiraDto> precoMedioCompraVendaCombustivelBancdeira();
	
	Map<String, List<Produto>> getExibeInformacaoImportadaSiglaPorRegiao();
	
	Map<String, List<Produto>> getExibeInformacaoImportadaDistribuidora();
	
	Map<Date, List<Produto>> getExibeInformacaoImportadaDataColeta();
}
