package br.com.indra.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class DataUtils {
	
	public static Date converteData(String valor, String formato) {
		Date data = null;
	    try {
	        DateFormat dtOutput = new SimpleDateFormat(formato);
	        data = dtOutput.parse(valor);
	    } catch (ParseException e) {
	        e.printStackTrace();
	    }
	    return data;
	}
}
