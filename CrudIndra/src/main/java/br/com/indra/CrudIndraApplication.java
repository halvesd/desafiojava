package br.com.indra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("br.com.indra")
public class CrudIndraApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudIndraApplication.class, args);		
	}
		
}
