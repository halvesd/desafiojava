package br.com.indra.responses;

import java.util.List;

public class Response<T> {
	
	private T dados;
	
	private List<String> listaErros;
	
	public Response(T dados) {
		this.dados = dados;
	}

	public Response(List<String> listaErros) {
		this.listaErros = listaErros;
	}

	public T getDados() {
		return dados;
	}

	public void setDados(T dados) {
		this.dados = dados;
	}

	public List<String> getListaErros() {
		return listaErros;
	}

	public void setListaErros(List<String> listaErros) {
		this.listaErros = listaErros;
	}

}
