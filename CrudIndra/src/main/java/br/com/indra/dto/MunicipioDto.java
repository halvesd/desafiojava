package br.com.indra.dto;

import lombok.Getter;
import lombok.Setter;

public class MunicipioDto {

	@Getter
	@Setter
	private String siglamunicipio;

	@Getter
	@Setter
	private Double valorcompra;

	@Getter
	@Setter
	private Double valorvenda;

	public MunicipioDto(String siglamunicipio, Double valorcompra) {
		this.siglamunicipio = siglamunicipio;
		this.valorcompra = valorcompra;
	}

	public MunicipioDto(String siglamunicipio, Double valorcompra, Double valorvenda) {
		this.siglamunicipio = siglamunicipio;
		this.valorcompra = valorcompra;
		this.valorvenda = valorvenda;
	}

}
