package br.com.indra.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

public class ProdutoDto {
	
	@Getter 
	@Setter
	private String siglaRegiao;	

	@Getter @Setter
	private String siglaestado;	
	
	@Getter @Setter
	private String siglamunicipio;
	
	@Getter @Setter
	private String distribuidora;
	
	@Getter @Setter
	private String coproduto;	
	
	@Getter @Setter
	private String noproduto;	
	
	@Getter @Setter
	private Date datacoleta;	
	
	@Getter @Setter
	private Double valorcompra;	
	
	@Getter @Setter
	private Double valorvenda;	
	
	@Getter @Setter
	private String unidademedida;	
	
	@Getter @Setter
	private String bandeira;

	public ProdutoDto() { }

	public ProdutoDto(String regiao, String siglaestado, String siglamunicipio, String distribuidora, String coproduto, String noproduto,
			Date datacoleta, Double valorcompra, Double valorvenda, String unidademedida, String bandeira) {
		this.siglaRegiao = regiao;
		this.siglaestado = siglaestado;
		this.siglamunicipio = siglamunicipio;
		this.distribuidora = distribuidora;
		this.coproduto = coproduto;
		this.noproduto = noproduto;
		this.datacoleta = datacoleta;
		this.valorcompra = valorcompra;
		this.valorvenda = valorvenda;
		this.unidademedida = unidademedida;
		this.bandeira = bandeira;
	}

	
}
