package br.com.indra.dto;

import lombok.Getter;
import lombok.Setter;

public class BandeiraDto {

	@Getter @Setter
	private String bandeira;
	
	@Getter @Setter
	private Double valorcompra;
	
	@Getter @Setter
	private Double valorvenda;
	
	public BandeiraDto(String bandeira, Double valorcompra, Double valorvenda) {
		this.bandeira = bandeira;
		this.valorcompra = valorcompra;
		this.valorvenda = valorvenda;
	}
	
}
