package br.com.indra.dto;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.br.CPF;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings({ "deprecation", "unused" })
@ApiModel(description = "Todos detalhes sobre o Usuario ")
public class UsuarioDto {

	@Getter @Setter
	private String nome;

	@Getter @Setter
	private String cpf;
	
	public UsuarioDto(String nome, String cpf) {
		this.nome = nome;
		this.cpf = cpf;
	}

}
