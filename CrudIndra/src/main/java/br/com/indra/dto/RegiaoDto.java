package br.com.indra.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class RegiaoDto {

	@Getter @Setter
	private String siglaregiao;
	
	@Getter @Setter
	private Double valorcompra;	
}
