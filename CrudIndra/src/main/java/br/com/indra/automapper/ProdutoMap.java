package br.com.indra.automapper;

import br.com.indra.dto.ProdutoDto;
import br.com.indra.entity.Produto;

public final class ProdutoMap {

	public static Produto ToProduto(ProdutoDto dto) {
		return new Produto(dto.getSiglaRegiao(), dto.getSiglaestado(), dto.getSiglamunicipio(), dto.getDistribuidora(), dto.getCoproduto(),
				dto.getNoproduto(), dto.getDatacoleta(), dto.getValorcompra(), dto.getValorvenda(), dto.getUnidademedida(), dto.getBandeira());
	}
}