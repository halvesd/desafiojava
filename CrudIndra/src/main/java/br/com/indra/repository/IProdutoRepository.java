package br.com.indra.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.indra.dto.BandeiraDto;
import br.com.indra.dto.MunicipioDto;
import br.com.indra.entity.Produto;

@Repository
public interface IProdutoRepository extends JpaRepository<Produto, Long> {

	@Query("select new br.com.indra.dto.MunicipioDto(p.siglamunicipio, avg(p.valorcompra)) from Produto p group by p.siglamunicipio")
	List<MunicipioDto> precoMedioCompraCombustivelMunicipio();

	@Query("select new br.com.indra.dto.MunicipioDto(p.siglamunicipio, avg(p.valorcompra), avg(p.valorvenda)) from Produto p group by p.siglamunicipio")
	List<MunicipioDto> precoMedioCompraVendaCombustivelMunicipio();

	@Query("select new br.com.indra.dto.BandeiraDto(p.bandeira, avg(p.valorcompra), avg(p.valorvenda)) from Produto p group by p.bandeira")
	List<BandeiraDto> precoMedioCompraVendaCombustivelBancdeira();
	
}
