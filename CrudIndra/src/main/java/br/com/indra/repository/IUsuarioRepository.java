package br.com.indra.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.indra.dto.UsuarioDto;
import br.com.indra.entity.Usuario;

@Repository
public interface IUsuarioRepository extends JpaRepository<Usuario, Long> {

	final String QUERY = "SELECT new br.com.indra.dto.UsuarioDto(c.nome,c.cpf) FROM Usuario c ";
	
	@Query(QUERY +" WHERE c.cpf = :paramCPF")
	UsuarioDto buscarPorCPF(@Param("paramCPF") String cpf);
		
}
