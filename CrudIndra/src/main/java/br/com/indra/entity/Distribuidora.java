package br.com.indra.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description="Nome fantazia / Razão Social da Distrbuidora.")
@Entity
@Table(name="Distribuidora")
public class Distribuidora extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Getter @Setter
	private String razaoSocial;
	
	@Getter @Setter
	private String nomeFantazia;
	
	@Getter @Setter
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bandeiraid", nullable = true)
	private Bandeira bandeira;
	
	@Getter @Setter
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "municipioid", nullable = true)
	private Municipio municipio;
	
	@Getter @Setter
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "estadoid", nullable = true)
	private Estado estado;		
	
	
}
