package br.com.indra.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description="Todos detalhes da bandeira.")
@Entity
@Table(name="Bandeira")
public class Bandeira extends BaseEntity {

	private static final long serialVersionUID = 1L;

	public Bandeira() {
		listaDistribuidoras = new HashSet<Distribuidora>(0);
	}

	@Getter @Setter
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "bandeira")
	private Set<Distribuidora> listaDistribuidoras;
	
	@Getter @Setter
	@Column(name="descriao", nullable = false, length = 50)
	private String descriao;
}
