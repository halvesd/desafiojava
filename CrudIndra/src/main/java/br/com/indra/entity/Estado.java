package br.com.indra.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description="Unidade de Federação.")
@Entity
@Table(name="Estado")
public class Estado extends BaseEntity {
	
	private static final long serialVersionUID = 1L;

	public Estado() {
		listaMunicipios = new HashSet<Municipio>(0);
	}

	@Getter @Setter
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "estado")
	private Set<Municipio> listaMunicipios;
		
	@Getter @Setter
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "estado")
	private Set<Distribuidora> listaDistribuidoras;

	@Getter @Setter
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "regiaoid", nullable = true)
	private Regiao regiao;		
	
	@Getter @Setter
	@Column(name="descriao", nullable = false, length = 40)
	private String descriao;

	@Getter @Setter
	@Column(name="siglaestado", nullable = false, length = 2)
	private String siglaestado;
	
}
