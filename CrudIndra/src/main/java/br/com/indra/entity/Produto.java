package br.com.indra.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description="Todos históricos dos combustiveis.")
@Entity
@Table(name="Produto")
public class Produto extends BaseEntity {
	
	private static final long serialVersionUID = 1L;
	
	@Getter @Setter
	@Column(name = "siglaregiao", nullable = true, length = 2)
	private String siglaregiao;
	
	@Getter @Setter
	@Column(name = "siglaestado", nullable = true, length = 2)
	private String siglaestado;
		
	@Getter @Setter
	@Column(name = "distribuidora", nullable = true, length = 70)
	private String distribuidora;
	
	@Getter @Setter
	@Column(name = "siglamunicipio", nullable = true, length = 50)
	private String siglamunicipio;
	
	@Getter @Setter
	@Column(name = "coproduto", nullable = true, length = 10)
	private String coproduto;
	
	@Getter @Setter
	@Column(name = "noproduto", nullable = true, length = 70)
	private String noproduto;
	
	@Getter @Setter
	@Column(name = "datacoleta")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date datacoleta;
	
	@Getter @Setter
	@Column(name = "valorcompra")
	private Double valorcompra;
	
	@Getter @Setter
	@Column(name = "valorvenda")
	private Double valorvenda;
	
	@Getter @Setter
	@Column(name = "unidademedida", nullable = true, length = 10)
	private String unidademedida;
	
	@Getter @Setter
	@Column(name = "bandeira", nullable = true, length = 40)
	private String bandeira;
		
	public Produto() { }

	public Produto(String siglaregiao, String siglaestado, String siglamunicipio, String distribuidora, String coproduto, String noproduto,
			Date datacoleta, Double valorcompra, Double valorvenda, String unidademedida, String bandeira) {
		this.siglaregiao = siglaregiao;
		this.siglaestado = siglaestado;
		this.siglamunicipio = siglamunicipio;
		this.distribuidora = distribuidora;
		this.coproduto = coproduto;
		this.noproduto = noproduto;
		this.datacoleta = datacoleta;
		this.valorcompra = valorcompra;
		this.valorvenda = valorvenda;
		this.unidademedida = unidademedida;
		this.bandeira = bandeira;
	}
	
}