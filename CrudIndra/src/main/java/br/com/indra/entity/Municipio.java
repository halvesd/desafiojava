package br.com.indra.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description="Município.")
@Entity
@Table(name="Municipio")
public class Municipio extends BaseEntity {

	private static final long serialVersionUID = 1L;
	
	public Municipio() {
		listaDistribuidoras = new HashSet<Produto>(0);
	}

	@Getter @Setter
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "distribuidora")
	private Set<Produto> listaDistribuidoras;
	
	@Getter @Setter
	@Column(name="descriao", nullable = false, length = 40)
	private String descricao;
	
	@Column(name="siglamunicipio", nullable = false, length = 40)
	private String siglamunicipio;
	
	@Getter @Setter
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "estadoid", nullable = true)
	private Estado estado;		
	
}
