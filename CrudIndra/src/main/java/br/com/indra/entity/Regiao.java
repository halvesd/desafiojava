package br.com.indra.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

@ApiModel(description="Todos detalhes sobre a região. ")
@Entity
@Table(name="Regiao")
public class Regiao extends BaseEntity {
	
	private static final long serialVersionUID = 1L;

	public Regiao() {
		listaEstados = new HashSet<Estado>(0);
	}

	@Getter @Setter
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "regiao")
	private Set<Estado> listaEstados;	
	
	@Getter @Setter
	@Column(name = "siglaregiao", nullable = true, length = 2)
	private String siglaregiao;
	
	@Getter @Setter
	@Column(name = "descricao", nullable = true, length = 30)
	private String descricao;
	
}
