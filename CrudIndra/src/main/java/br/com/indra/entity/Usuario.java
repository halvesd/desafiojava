package br.com.indra.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.br.CPF;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("deprecation")
@ApiModel(description="Todos detalhes sobre o Usuario. ")
@Entity
@Table(name="Usuario")
public class Usuario extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Getter @Setter
	@ApiModelProperty(notes="O nome deve ter pelo menos três caracteres")
	@Size(min=2, message="O nome deve ter pelo menos três caracteres")
	@NotEmpty(message = "nome do cliente não pode ser vazio")
	@Column(name="nome", nullable = false, length = 50)
	public String nome;
	
	@Getter @Setter
	@NotEmpty(message = "CPF do usuário não pode ser vazio")
	@CPF(message = "CPF inválido")
	@Column(name="CPF", nullable = false, length = 11)
	public String 	cpf;
	
	public Usuario() { }
	
}