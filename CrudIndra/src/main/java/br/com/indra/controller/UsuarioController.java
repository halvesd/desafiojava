package br.com.indra.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.indra.dto.UsuarioDto;
import br.com.indra.entity.Usuario;
import br.com.indra.responses.Response;
import br.com.indra.service.IUsuarioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

// @CrossOrigin(origins = "4200", allowedHeaders = "*")
@Api(value = "APICadastroUsuario", description = "API - Cadastro de usuário")
@RestController
@RequestMapping(path = "api")
public class UsuarioController {

	private IUsuarioService iUsuarioService;
	
	@Autowired
	public UsuarioController(IUsuarioService iUsuarioService) {
		this.iUsuarioService = iUsuarioService;
	}
	
	@ApiOperation(value = "Exibe uma lista de usuário", response = Usuario[].class)
	@RequestMapping(value = "usuario", method= RequestMethod.GET)
	public ResponseEntity<Response<List<Usuario>>> listarTodos(){
		return ResponseEntity.ok(new Response<List<Usuario>>(iUsuarioService.listarTodos()));
	}
	
	@ApiOperation(value = "Exibe um usuário único", notes = "Retorna o usuário")
	@RequestMapping(value = "usuario/{id}", method= RequestMethod.GET)
	public ResponseEntity<Response<Usuario>> buscarPorId(@PathVariable(name = "id") Long id){
		return ResponseEntity.ok(new Response<Usuario>(iUsuarioService.buscarPorId(id)));
	}

	@ApiOperation(value = "Exibe um cliente único por CPF", notes = "Retorna o usuário por CPF")
	@RequestMapping(value = "usuario/{cpf}", method= RequestMethod.GET)
	public ResponseEntity<Response<UsuarioDto>> buscarPorCPF(@PathVariable(name = "cpf") String cpf){
		if ( null == iUsuarioService.buscarPorCPF(cpf) ){
			List<String> erros = new ArrayList<String>();
			erros.add("CPF não encontrado...");
			return ResponseEntity.badRequest().body(new Response<UsuarioDto>(erros));
		}
		return ResponseEntity.ok(new Response<UsuarioDto>(iUsuarioService.buscarPorCPF(cpf)));
	}
	
	@ApiOperation(value = "Cadastra usuário")
	@RequestMapping(value = "usuario", method= RequestMethod.POST)
	public ResponseEntity<Response<Usuario>> cadastrar(@RequestBody @Valid Usuario usuario, BindingResult resul) {
		if (resul.hasErrors()) {
			List<String> erros = new ArrayList<String>();
			resul.getAllErrors().forEach(erro -> {
				erros.add(erro.getDefaultMessage());
			});
			return ResponseEntity.badRequest().body(new Response<Usuario>(erros));
		}
		return ResponseEntity.ok(new Response<Usuario>(iUsuarioService.cadastrar(usuario)));
	}
	
	@ApiOperation(value = "Atualiza usuário")
	@RequestMapping(value = "usuario/{id}", method= RequestMethod.PUT)
	public ResponseEntity<Response<Usuario>> atualizar(@PathVariable(name = "id") Long id, @RequestBody @Valid Usuario usuario, BindingResult resul) {
		if (resul.hasErrors()) {
			List<String> erros = new ArrayList<String>();
			resul.getAllErrors().forEach(erro -> {
				erros.add(erro.getDefaultMessage());
			});
			return ResponseEntity.badRequest().body(new Response<Usuario>(erros));
		}
		return ResponseEntity.ok(new Response<Usuario>(iUsuarioService.atualizar(usuario)));
	}
	
	@ApiOperation(value = "Remove usuário")
	@RequestMapping(value = "usuario/{id}", method= RequestMethod.DELETE)
	public ResponseEntity<Response<Integer>> remover(@PathVariable(name = "id") Long id){
		iUsuarioService.remover(id);
		return ResponseEntity.ok(new Response<Integer>(1));
	}

}