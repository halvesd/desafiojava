package br.com.indra.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.indra.automapper.ProdutoMap;
import br.com.indra.dto.BandeiraDto;
import br.com.indra.dto.MunicipioDto;
import br.com.indra.entity.Produto;
import br.com.indra.responses.Response;
import br.com.indra.service.IProdutoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

//@CrossOrigin(origins = "4200", allowedHeaders = "*")
@Api(value = "APICadastroProduto", description = "API - Cadastro de produto")
@RestController
@RequestMapping(path = "api")
public class ProdutoController {

	private IProdutoService iProdutoService;

	@Autowired
	public ProdutoController(IProdutoService iProdutoService) {
		this.iProdutoService = iProdutoService;
	}

	@ApiOperation(value = "Exibe uma lista de produtos", response = Produto[].class)
	@RequestMapping(value = "produto", method = RequestMethod.GET)
	public ResponseEntity<Response<List<Produto>>> listarTodos() {
		return ResponseEntity.ok(new Response<List<Produto>>(iProdutoService.listarTodos()));
	}

	@ApiOperation(value = "Exibe produto único", notes = "Retorna o produtos")
	@RequestMapping(value = "produto/{id}", method = RequestMethod.GET)
	public ResponseEntity<Response<Produto>> buscarPorId(@PathVariable(name = "id") Long id) {
		return ResponseEntity.ok(new Response<Produto>(iProdutoService.buscarPorId(id)));
	}

	@ApiOperation(value = "Cadastra produto")
	@RequestMapping(value = "produto", method = RequestMethod.POST)
	public ResponseEntity<Response<Produto>> cadastrar(@RequestBody @Valid Produto Produto, BindingResult resul) {
		if (resul.hasErrors()) {
			List<String> erros = new ArrayList<String>();
			resul.getAllErrors().forEach(erro -> {
				erros.add(erro.getDefaultMessage());
			});
			return ResponseEntity.badRequest().body(new Response<Produto>(erros));
		}
		return ResponseEntity.ok(new Response<Produto>(iProdutoService.cadastrar(Produto)));
	}

	@ApiOperation(value = "Atualiza produto")
	@RequestMapping(value = "produto/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Response<Produto>> atualizar(@PathVariable(name = "id") Long id,
			@RequestBody @Valid Produto Produto, BindingResult resul) {
		if (resul.hasErrors()) {
			List<String> erros = new ArrayList<String>();
			resul.getAllErrors().forEach(erro -> {
				erros.add(erro.getDefaultMessage());
			});
			return ResponseEntity.badRequest().body(new Response<Produto>(erros));
		}
		return ResponseEntity.ok(new Response<Produto>(iProdutoService.atualizar(Produto)));
	}

	@ApiOperation(value = "Remove produto")
	@RequestMapping(value = "produto/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Response<Integer>> remover(@PathVariable(name = "id") Long id) {
		iProdutoService.remover(id);
		return ResponseEntity.ok(new Response<Integer>(1));
	}

	@ApiOperation(value = "Importa arquivo csv")

	@RequestMapping(value = "/importacsv", method = RequestMethod.POST)
	public ResponseEntity<Response<Integer>> importacsv(@RequestParam("arquivo") MultipartFile file,
			HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes)
			throws SQLException, IOException {
		iProdutoService.importaArquivoCsv(file).forEach(dto -> {			
			iProdutoService.cadastrar(ProdutoMap.ToProduto(dto));						
		});
		return ResponseEntity.ok(new Response<Integer>(1));
	}

	@ApiOperation(value = "Exibe o preço médio da compra do decombustível com base no nome do munícipio", response = Produto[].class)

	@RequestMapping(value = "precomediocombustivelmunicipio", method = RequestMethod.GET)
	public ResponseEntity<Response<List<MunicipioDto>>> precoMedioCompraCombustivelMunicipio() {
		return ResponseEntity
				.ok(new Response<List<MunicipioDto>>(iProdutoService.precoMedioCompraCombustivelMunicipio()));
	}

	@ApiOperation(value = "Exibe o preço médio da compra e venda do decombustível com base no nome do munícipio", response = Produto[].class)
	@RequestMapping(value = "precomediocompravendacombustivelmunicipio", method = RequestMethod.GET)
	public ResponseEntity<Response<List<MunicipioDto>>> precoMedioCompraVendaCombustivelMunicipio() {
		return ResponseEntity
				.ok(new Response<List<MunicipioDto>>(iProdutoService.precoMedioCompraVendaCombustivelMunicipio()));
	}

	@ApiOperation(value = "Exibe o preço médio da compra e venda do decombustível com base no nome da bandeira", response = Produto[].class)
	@RequestMapping(value = "precomediocompravendacombustivelbandeira", method = RequestMethod.GET)
	public ResponseEntity<Response<List<BandeiraDto>>> precoMedioCompraVendaCombustivelBancdeira() {
		return ResponseEntity
				.ok(new Response<List<BandeiraDto>>(iProdutoService.precoMedioCompraVendaCombustivelBancdeira()));
	}

	@ApiOperation(value = "Exibe todas as informações importadas por sigla da região", response = Produto[].class)
	@RequestMapping(value = "exibeinformacaoimportadasiglaregiao", method = RequestMethod.GET)
	public ResponseEntity<Response<Map<String, List<Produto>>>> exibeInformacaoImportadaSiglaPorRegiao() {
		return ResponseEntity
				.ok(new Response<Map<String, List<Produto>>> (iProdutoService.getExibeInformacaoImportadaSiglaPorRegiao()));
	}

	@ApiOperation(value = "Exibe todas as informações importadas por data de coleta", response = Produto[].class)
	@RequestMapping(value = "exibeinformacaoimportadadatacoleta", method = RequestMethod.GET)
	public ResponseEntity<Response<Map<Date, List<Produto>>>> exibeInformacaoImportadaDataColeta() {
		return ResponseEntity
				.ok(new Response<Map<Date, List<Produto>>>(iProdutoService.getExibeInformacaoImportadaDataColeta()));
	}
	
	@ApiOperation(value = "Exibe todas as informações agrupadas por distribuidora", response = Produto[].class)
	@RequestMapping(value = "exibeinformacaoimportadadistribuidora", method = RequestMethod.GET)
	public ResponseEntity<Response<Map<String, List<Produto>>>> exibeInformacaoImportadaDistribuidora() {
		return ResponseEntity
				.ok(new Response<Map<String, List<Produto>>> (iProdutoService.getExibeInformacaoImportadaDistribuidora()));
	}
	
	
}
